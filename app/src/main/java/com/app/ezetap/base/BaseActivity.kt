package com.app.ezetap.base

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.app.ezetap.R
import com.app.ezetap.listener.AlertSheetListener
import com.app.ezetap.util.afterTextChanged
import com.app.ezetap.view.AlertSheet
import com.app.ezetap.view.ProgressDialog
import com.app.ezetap.view.activity.DisplayActivity
import com.app.service.model.Uidata
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import java.lang.Exception

abstract class BaseActivity : AppCompatActivity() {

    private val progressDialog = ProgressDialog.getInstance()
    private var errorBottomSheetFragment: AlertSheet? = null

    abstract fun setOnclickListener()

    abstract fun dataObserver()

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

    }

    override fun onResume() {
        super.onResume()
        setOnclickListener()
        dataObserver()
    }

    protected fun showProgress(show: Boolean) {
        if (show) {
            progressDialog.show(supportFragmentManager, "Progress")
        } else {
            progressDialog.dismissDialog()
        }
    }

    protected fun showErrorView(
        title: String, message: String,
        callback: () -> Unit
    ) {
        errorBottomSheetFragment?.dismiss()
        errorBottomSheetFragment = AlertSheet.getInstance(title, message, object :
            AlertSheetListener {
            override fun onItemClick() {
                callback.invoke()
            }
        })
        errorBottomSheetFragment?.isCancelable = false
        errorBottomSheetFragment?.show(supportFragmentManager, "dialog")
    }

    protected fun showSnackBar(message: String, view: View) {
        try {
            val snack = Snackbar.make(
                    view,
                    message,
                    Snackbar.LENGTH_SHORT
            )
            snack.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun generateLogoView(logoUrl: String?): AppCompatImageView {
        val logoView = AppCompatImageView(this)
        logoView.scaleType = ImageView.ScaleType.FIT_XY
        Glide.with(this)
                .load(logoUrl)
                .placeholder(R.drawable.ic_baseline_broken_image_24)
                .into(logoView)
        return logoView
    }

    fun generateHeadingView(headingText: String?): AppCompatTextView {
        val headingView = AppCompatTextView(this)
        headingView.text = headingText
        headingView.textSize = 30f
        headingView.gravity = Gravity.CENTER_HORIZONTAL
        headingView.setTextColor(ContextCompat.getColor(this, R.color.black))
        return headingView
    }


    fun generateButtonView(uidata: Uidata): AppCompatButton {
        val btnView = AppCompatButton(this)
        btnView.text = uidata.value
        btnView.textSize = 20f
        btnView.background = ContextCompat.getDrawable(this, R.drawable.btn_green_bg)
        btnView.setTextColor(ContextCompat.getColor(this, R.color.white))
        btnView.gravity = Gravity.CENTER_HORIZONTAL
        return btnView
    }

    fun generateEditTextView(uidata: Uidata): AppCompatEditText {
        val editView = AppCompatEditText(this)
        editView.hint = uidata.hint
        editView.textSize = 20f
        editView.inputType = getInputType(uidata.key)
        editView.imeOptions = EditorInfo.IME_ACTION_DONE
        editView.isSingleLine = true
        editView.gravity = Gravity.START
        editView.setTextColor(ContextCompat.getColor(this, R.color.black))
        return editView
    }

    private fun getInputType(uitype: String?): Int {
        return when (uitype){
            "text_phone" ->{
                InputType.TYPE_CLASS_PHONE
            }
            else ->{
                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
            }
        }
    }

}