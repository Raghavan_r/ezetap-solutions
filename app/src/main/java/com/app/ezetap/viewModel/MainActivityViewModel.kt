package com.app.ezetap.viewModel

import android.text.TextUtils
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.ezetap.liadatautil.toSingleEvent
import com.app.service.handler.ResultWrapper
import com.app.service.model.UIModel
import com.app.service.repo.DataRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivityViewModel @ViewModelInject constructor(
        private val dataRepo: DataRepo
) : ViewModel() {

    private val _apiError = MutableLiveData<String>()
    val apiError: LiveData<String> = _apiError.toSingleEvent()


    private val _validationError = MutableLiveData<String?>()
    val validationError: LiveData<String?> = _validationError.toSingleEvent()

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading.toSingleEvent()

    private val _uiData = MutableLiveData<UIModel?>()
    val uiData: LiveData<UIModel?> = _uiData.toSingleEvent()

    init {
        getUIData()
    }

    fun getUIData() {
        viewModelScope.launch(Dispatchers.Main) {
            _isLoading.value = true
            val response = dataRepo.getUiData()
            when (response) {
                is ResultWrapper.Success -> {
                    _isLoading.postValue(false)
                    _uiData.postValue(response.value)
                }
                is ResultWrapper.NetworkError -> {
                    _isLoading.postValue(false)
                    _apiError.postValue(response.value)
                }
                is ResultWrapper.GenericError -> {
                    _isLoading.postValue(false)
                    _apiError.postValue(response.value)
                }
            }
        }
    }

    fun validateUser(): Boolean {
        var isAllValid = true
        _uiData.value?.uidata?.forEach {
            if (it.uitype.equals("edittext", true) && TextUtils.isEmpty(it.updatedData)) {
                isAllValid = false
                _validationError.value = it.hint
                return isAllValid
            }
        }
        return isAllValid
    }
}