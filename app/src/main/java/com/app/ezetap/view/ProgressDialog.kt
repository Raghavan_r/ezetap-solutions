package com.app.ezetap.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.app.ezetap.databinding.ProgressDialogBinding

class ProgressDialog : DialogFragment(){
    private lateinit var binding: ProgressDialogBinding

    companion object {

        private var confirmDialog: ProgressDialog? = null

        fun getInstance(): ProgressDialog {
            if (confirmDialog == null) {
                confirmDialog = ProgressDialog()
            }
            return confirmDialog as ProgressDialog
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = ProgressDialogBinding.inflate(
            inflater,
            container,
            false
        )

        this.binding = binding

        if (dialog != null && dialog?.window != null) {
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        }
        dialog?.setCanceledOnTouchOutside(true)

        return binding.root
    }

    fun dismissDialog() {
        if (dialog != null) {
            dismiss()
        }
    }


}