package com.app.ezetap.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.app.ezetap.R
import com.app.ezetap.base.BaseActivity
import com.app.ezetap.databinding.ActivityDisplayBinding
import com.app.service.model.UIModel
import com.app.service.model.Uidata

class DisplayActivity : BaseActivity() {

    private lateinit var binding: ActivityDisplayBinding
    private val layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)

    override fun setOnclickListener() {

    }

    override fun dataObserver() {
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDisplayBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val data = intent.getParcelableExtra<UIModel>(ARG_UI_DATA)
        renderUIData(data)
    }

    private fun renderUIData(it: UIModel?) {
        val logo = generateLogoView(it?.logoUrl)
        binding.layMain.addView(logo, ViewGroup.LayoutParams.MATCH_PARENT, 250)
        val header = generateHeadingView(it?.headingText)
        layoutParams.setMargins(30, 20, 30, 0)
        binding.layMain.addView(header, layoutParams)
        generateBottomData(it?.uidata)
    }

    private fun generateBottomData(uidata: List<Uidata>?) {
        uidata?.forEach {
            when (it.uitype) {
                "label" -> {
                    binding.layMain.addView(generateTextView(it), layoutParams)
                }
                "edittext" -> {
                    val  editView = generateEditTextView(it)
                    editView.setText(it.updatedData)
                    editView.isEnabled = false
                    binding.layMain.addView(editView, layoutParams)
                }
            }
        }

    }

    private fun generateTextView(uidata: Uidata): AppCompatTextView {
        val textView = AppCompatTextView(this)
        textView.text = uidata.value
        textView.textSize = 20f
        layoutParams.setMargins(30, 30, 30, 0)
        textView.gravity = Gravity.START
        textView.setTextColor(ContextCompat.getColor(this, R.color.black))
        return textView
    }

}