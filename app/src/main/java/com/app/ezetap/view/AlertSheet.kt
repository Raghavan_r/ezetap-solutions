package com.app.ezetap.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.app.ezetap.R
import com.app.ezetap.databinding.AlertSheetBinding
import com.app.ezetap.listener.AlertSheetListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class AlertSheet (
    private var title : String,
    private var message :String,
    private var listener: AlertSheetListener
) : BottomSheetDialogFragment(){

    private lateinit var binding: AlertSheetBinding

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?):
            Dialog = BottomSheetDialog(requireContext(), theme)


    companion object {

        private var confirmDialog: AlertSheet? = null

        fun getInstance(title:String,message:String,listener : AlertSheetListener): AlertSheet {
            if (confirmDialog == null) {
                confirmDialog = AlertSheet(title,message,listener)
            }
            this.confirmDialog?.title = title
            this.confirmDialog?.message = message
            this.confirmDialog?.listener = listener
            return confirmDialog as AlertSheet
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.alert_sheet,
            container,
            false
        )
        binding.lifecycleOwner = this

        setActionListener()
        return binding.root
    }

    private fun setActionListener() {

        binding.tvTitle.text = title

        binding.tvDesc.text = message


        binding.btnOk.setOnClickListener {
            listener.onItemClick()
            dismiss()
        }

    }

}