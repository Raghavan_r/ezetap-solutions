package com.app.ezetap.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.app.ezetap.R
import com.app.ezetap.base.BaseActivity
import com.app.ezetap.databinding.ActivityMainBinding
import com.app.ezetap.util.afterTextChanged
import com.app.ezetap.viewModel.MainActivityViewModel
import com.app.service.model.UIModel
import com.app.service.model.Uidata
import dagger.hilt.android.AndroidEntryPoint

const val ARG_UI_DATA = "ui_data"

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    private val layoutParams = LinearLayout.LayoutParams(
            MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun setOnclickListener() {
    }

    override fun dataObserver() {

        viewModel.apiError.observe(this, {
            showErrorView(getString(R.string.api_error), it) {
                viewModel.getUIData()
            }
        })

        viewModel.isLoading.observe(this, {
            showProgress(it)
        })

        viewModel.validationError.observe(this, {
            if (it != null) {
                showSnackBar(it,binding.root)
            }
        })

        viewModel.uiData.observe(this, {
            renderUIData(it)
        })
    }

    private fun renderUIData(it: UIModel?) {
        val logo = generateLogoView(it?.logoUrl)
        binding.layMain.addView(logo, MATCH_PARENT, 250)
        val header = generateHeadingView(it?.headingText)
        layoutParams.setMargins(30, 20, 30, 0)
        binding.layMain.addView(header, layoutParams)
        generateBottomData(it?.uidata)
    }


    private fun generateBottomData(uidata: List<Uidata>?) {
        uidata?.forEach {
            when (it.uitype) {
                "label" -> {
                    val textView = generateTextView(it)
                    binding.layMain.addView(textView, layoutParams)


                }
                "edittext" -> {
                    val  editView = generateEditTextView(it)
                    editView.afterTextChanged {value->
                        it.updatedData = value
                    }
                    binding.layMain.addView(editView, layoutParams)
                }
                "button" -> {
                    val view = generateButtonView(it)
                    view.setOnClickListener {
                        if (viewModel.validateUser()){
                            val dataIntent = Intent(this,DisplayActivity::class.java)
                            dataIntent.putExtra(ARG_UI_DATA, viewModel.uiData.value)
                            startActivity(dataIntent)
                        }
                    }
                    binding.layMain.addView(view, layoutParams)
                }

            }
        }

    }

    private fun generateTextView(uidata: Uidata): AppCompatTextView {
        val textView = AppCompatTextView(this)
        textView.textSize = 20f
        textView.text = uidata.value
        layoutParams.setMargins(30, 30, 30, 0)
        textView.gravity = Gravity.START
        textView.setTextColor(ContextCompat.getColor(this, R.color.black))
        return textView

    }


}

