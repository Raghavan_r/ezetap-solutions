package com.app.service.network

import android.content.Context
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

const val TIME_OUT = 20L

class HttpClientBuilderFactory {

    private val okHttpClient by lazy { OkHttpClient() }

    fun create(interceptor: Interceptor): OkHttpClient.Builder = okHttpClient.newBuilder().apply {
        connectTimeout(TIME_OUT, TimeUnit.SECONDS)
        readTimeout(TIME_OUT, TimeUnit.SECONDS)
        addInterceptor(interceptor)
        addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
    }
}