package com.app.service.network

import android.text.TextUtils
import java.lang.Exception

class ApiFailureException(message : String?= null, cause : Throwable? = null) : Exception(message){

    private val _message: String? by lazy {
        if (!TextUtils.isEmpty(message)) {
            cause?.toString()
        } else {
            null
        }
    }


    override val message get() = _message ?: super.message

    init {
        if (cause != null) {
            super.initCause(cause)
        }
    }

}