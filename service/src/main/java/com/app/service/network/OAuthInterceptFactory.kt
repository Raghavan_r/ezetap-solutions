package com.app.service.network

import android.os.Build
import androidx.annotation.RequiresApi
import okhttp3.Interceptor
import okhttp3.Response

@RequiresApi(Build.VERSION_CODES.KITKAT)
class OAuthInterceptFactory : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(addHeaderInfo(chain))
    }

    private fun addHeaderInfo(chain: Interceptor.Chain) =
        chain.request().newBuilder()
        .build()

}
