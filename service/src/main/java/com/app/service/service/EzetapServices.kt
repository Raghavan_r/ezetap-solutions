package com.app.service.service

import com.app.service.model.UIModel
import com.app.service.model.Uidata
import com.app.service.network.HttpClientBuilderFactory
import com.app.service.network.OAuthInterceptFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

const val END_POINT = "mobileapps/android_assignment.json"
const val BASE_URL = "https://demo.ezetap.com/"

interface EzetapServices {

    @GET(END_POINT)
    suspend fun getUiData(): UIModel

    companion object {

        fun createService(
            httpClient: HttpClientBuilderFactory,
            oAuthInterceptor: OAuthInterceptFactory
        ): EzetapServices {

            val okHttpClient = httpClient.create(oAuthInterceptor).build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EzetapServices::class.java)
        }
    }

}