package com.app.service.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UIModel(
    @SerializedName("heading-text")
    val headingText: String?,
    @SerializedName("logo-url")
    val logoUrl: String?,
    @SerializedName("uidata")
    val uidata: List<Uidata>?
) : Parcelable

@Parcelize
data class Uidata(
    @SerializedName("hint")
    val hint: String?,
    @SerializedName("key")
    val key: String?,
    @SerializedName("uitype")
    val uitype: String?,
    @SerializedName("value")
    val value: String?,
    var updatedData :String?
) : Parcelable