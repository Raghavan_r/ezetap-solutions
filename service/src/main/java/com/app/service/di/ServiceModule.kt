package com.app.service.di

import android.os.Build
import androidx.annotation.RequiresApi
import com.app.service.network.HttpClientBuilderFactory
import com.app.service.network.OAuthInterceptFactory
import com.app.service.service.EzetapServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@RequiresApi(Build.VERSION_CODES.KITKAT)
@Module
@InstallIn(ApplicationComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun providesAuthService(
        httpClient: HttpClientBuilderFactory,
        oAuthInterceptor: OAuthInterceptFactory
    ): EzetapServices = EzetapServices.createService(httpClient, oAuthInterceptor)


    @Provides
    @Singleton
    fun provideOAuthInterceptorFactory(): OAuthInterceptFactory =
        OAuthInterceptFactory()

    @Provides
    @Singleton
    fun provideHttpBuilderFactory(): HttpClientBuilderFactory =
        HttpClientBuilderFactory()

}