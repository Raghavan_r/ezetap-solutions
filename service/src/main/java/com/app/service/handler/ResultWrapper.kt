package com.app.service.handler


sealed class ResultWrapper<out T>{


    data class Success<out T>(val value: T): ResultWrapper<T>()
    data class GenericError(val value: String): ResultWrapper<Nothing>()
    data class NetworkError(val value: String): ResultWrapper<Nothing>()
}