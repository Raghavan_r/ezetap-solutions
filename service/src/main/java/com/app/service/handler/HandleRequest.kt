package com.app.service.handler

import android.util.Log
import java.lang.Exception
import java.net.UnknownHostException


private const val genericErrorMsg = "Something went wrong try again later!"
private const val networkErrorMsg = "Check your internet connection!"

suspend fun <T : Any> handleRequest(requestFunc: suspend () -> T): ResultWrapper<T>? {

    return try {
        ResultWrapper.Success(requestFunc.invoke())

    } catch (e: Exception) {
        handleException(e)
    }
}

private fun <T> handleException(throwable: Exception): ResultWrapper<T> {
    Log.e("Service Error", throwable.message ?: "")
    return when (throwable) {
        is UnknownHostException -> ResultWrapper.NetworkError(networkErrorMsg)
        else -> {
            ResultWrapper.GenericError(genericErrorMsg)
        }
    }
}
