package com.app.service.repo

import com.app.service.handler.handleRequest
import com.app.service.service.EzetapServices
import javax.inject.Inject

class DataRepo @Inject constructor(private val ezeTapServices : EzetapServices ){

    suspend fun getUiData() = handleRequest{
        ezeTapServices.getUiData()
    }

}